#!/bin/bash

composer install --ignore-platform-reqs
yarn install
/usr/bin/php artisan cache:clear
/usr/bin/php artisan route:clear
/usr/bin/php artisan config:clear
/usr/bin/php artisan view:clear
/usr/bin/php artisan migrate -n
/usr/bin/php artisan serve --host=0.0.0.0 --port=${PROJECT_PORT}

# RAYLARADOCK-GENERATOR

This project is a generator of laravel project using docker & docker-compose.

PhP Version  : `8.0`

Node Version : `16.x`

## Requirements

- docker
- docker-compose

## How to install

Open `.env` file and modify variables value :

### Project configuration
`PROJECT_NAME`: Is the name of project (Update container_name, directory of project in container)

### Database configuration
`PG_DATABASE`: The name of your database

`PG_USER`: The username of postgres who can access to database

`PG_PASSWORD`: The password of `PG_USER` (default: `password`)

`PG_PORT`: Port to access to postgres (default: `5432`)

`PG_HOST`: Host of postgres (default: `db`)

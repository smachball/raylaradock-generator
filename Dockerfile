FROM debian:buster

ARG LOCAL_USER=project
ARG PROJECT_NAME=project
ARG PROJECT_PORT=8000
ARG PG_DATABASE=postgres
ARG PG_USER=postgres
ARG PG_PASSWORD=postgres
ARG PG_PORT=5432
ARG PG_HOST=db

ENV LOCAL_USER=$LOCAL_USER
ENV PROJECT_NAME=$PROJECT_NAME
ENV PROJECT_PORT=$PROJECT_PORT
ENV PG_DATABASE=$PG_DATABASE
ENV PG_USER=$PG_USER
ENV PG_PASSWORD=$PG_PASSWORD
ENV PG_PORT=$PG_PORT
ENV PG_HOST=$PG_HOST

ENV PROJECT_PATH=/opt/$PROJECT_NAME

#RUN addgroup $LOCAL_USER && useradd -rm -d /opt/$PROJECT_NAME -s /bin/bash -g $LOCAL_USER -u 1001 ${LOCAL_USER} && chown -hR $LOCAL_USER:$LOCAL_USER /opt/$PROJECT_NAME
RUN useradd --create-home --home-dir $PROJECT_PATH $LOCAL_USER && \
mkdir -p $HOME && \
chown -R $LOCAL_USER:$LOCAL_USER $PROJECT_PATH

RUN apt update -yqq && apt install -yqq bash curl wget gnupg gnupg2 gnupg1 lsb-release && \
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/sury-php.list && \
wget -qO - https://packages.sury.org/php/apt.gpg | apt-key add - && \
apt -yqq update && \
apt install -yqq php8.0 php8.0-opcache libapache2-mod-php8.0 php8.0-mysql php8.0-curl php8.0-gd php8.0-intl php8.0-mbstring php8.0-xml php8.0-zip php8.0-fpm php8.0-readline php8.0-pgsql

RUN curl -sL https://deb.nodesource.com/setup_16.x | bash - && \ 
apt -yqq update && \
apt -yqq install nodejs && \
npm install -g yarn && \
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
php -r "if (hash_file('sha384', 'composer-setup.php') === '55ce33d7678c5a611085589f1f3ddf8b3c52d662cd01d4ba75c0ee0459970c2200a51f492d557530c71c15d8dba01eae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
php composer-setup.php && \
php -r "unlink('composer-setup.php');" && \
mv composer.phar /usr/local/bin/composer

RUN mkdir /opt/new_app && chown -hR $LOCAL_USER:$LOCAL_USER /opt/new_app && \
cd /opt/new_app && composer create-project laravel/laravel . && \
chown -hR $LOCAL_USER:$LOCAL_USER /opt/new_app

WORKDIR $PROJECT_PATH
USER $LOCAL_USER

COPY ./app/entrypoint.sh $PROJECT_PATH

SHELL ["/bin/bash", "-c"]

CMD if [ $(ls /opt/$PROJECT_NAME | wc -l) -gt 2 ]; then cd /opt/$PROJECT_NAME && bash ./entrypoint.sh; else cp -r /opt/new_app/* $PROJECT_PATH && cd /opt/$PROJECT_NAME && bash ./entrypoint.sh ; fi

